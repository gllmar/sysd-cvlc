# sysd-qlcplus

## Install via curl

```
mkdir -p ~/.config/systemd/user && curl -o ~/.config/systemd/user/cvlc@.service https://gitlab.com/gllmar/sysd-cvlc/-/raw/main/cvlc@.service?ref_type=heads && systemctl --user daemon-reload
```

## Configure, Enable, Start a specific instance

```
read -p "Enter movie file full Path: " movie_path && systemctl --user restart cvlc@$(systemd-escape "$movie_path") && systemctl --user enable cvlc@$(systemd-escape "$movie_path")

```

## Stop all instances

```
for service in $(systemctl --user list-units --type=service | grep 'cvlc@' | awk '{print $1}'); do systemctl --user stop $service; done
```


## Disable all instances

```
for service in $(systemctl --user list-units --type=service | grep 'cvlc@' | awk '{print $1}'); do systemctl --user disable $service; done
```


## Uninstall

``` 
for service in $(systemctl --user list-units --type=service | grep 'cvlc@' | awk '{print $1}'); do systemctl --user stop $service; done && for service in $(systemctl --user list-units --type=service | grep 'cvlc@' | awk '{print $1}'); do systemctl --user disable $service; done && rm ~/.config/systemd/user/cvlc@.service
```


